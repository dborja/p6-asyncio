import asyncio
async def counter(id, time):
    print(f"counter {id} starting...")
    await asyncio.sleep(1)

    for i in range(time):
        print(f"Counter {id}: {i+1}")
        if i+1 == time:
            print(f"Counter {id} finishing...")
        await asyncio.sleep(1)

async def main():
    await asyncio.gather(counter('A', 4), counter('B',2 ), counter('C',6))

asyncio.run(main())
