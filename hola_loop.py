import asyncio

async def main():
  print('hola...')
  await asyncio.sleep(1)
  print('...mundo')

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.close()
