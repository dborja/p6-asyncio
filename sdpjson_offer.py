import json
import asyncio
import sdp_transform

sdp_doc = sdp_transform.parse(open("sdp_doc_offer.sdp").read())
sdp_offer = sdp_transform.write(sdp_doc)

message = {
    "type": "offer",
    "sdp": sdp_offer
}

message_json = json.dumps(message)
class EchoClientProtocol:
    def __init__(self, sdp_offer, on_con_lost):
        self.message = sdp_offer
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        print('Send:', message_json)
        self.transport.sendto(message_json.encode())

    def datagram_received(self, data, addr):
        print("Received:", data.decode())

        print("Close the socket")
        self.transport.close()

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)


async def main():
    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    on_con_lost = loop.create_future()
    message = sdp_offer

    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoClientProtocol(message, on_con_lost),
        remote_addr=('127.0.0.1', 9999))

    try:
        await on_con_lost
    finally:
        transport.close()


asyncio.run(main())